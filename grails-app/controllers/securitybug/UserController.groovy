package securitybug


import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.authentication.dao.NullSaltSource
import grails.plugin.springsecurity.ui.RegistrationCode

class UserController extends grails.plugin.springsecurity.ui.UserController {

    def mailService

    def save() {
        def user = lookupUserClass().newInstance(params)
        if (!user.save(flush: true)) {
            render view: 'create', model: [user: user, authorityList: sortedRoles()]
            return
        }

        //generate secured link
        def registrationCode = new RegistrationCode(username: user.username).save(flush: true)
        String url = generateLink('verifyRegistration', [t: registrationCode.token])
        def conf = SpringSecurityUtils.securityConfig

        //here we should send an invitation mail
        mailService.sendMail {
            async true
            to user.email
            from conf.ui.register.emailFrom
            subject conf.ui.register.emailSubject
            html url
        }

        addRoles(user)
        flash.message = "${message(code: 'default.created.message', args: [message(code: 'user.label', default: 'User'), user.id])}"
        redirect action: 'edit', id: user.id
    }



    def verifyRegistration() {
        def conf = SpringSecurityUtils.securityConfig
        String defaultTargetUrl = conf.successHandler.defaultTargetUrl
        String token = params.t
        println token

        def registrationCode = token ? RegistrationCode.findByToken(token) : null
        if (!registrationCode) {
            println registrationCode
            flash.error = message(code: 'spring.security.ui.register.badCode')
            redirect uri: defaultTargetUrl
            return
        }

        render(view: "initpassword", model: [token: token])
    }

    def savePassword() {
        println "savePassword is invoked"
        String token = params.token
        def conf = SpringSecurityUtils.securityConfig
        String defaultTargetUrl = conf.successHandler.defaultTargetUrl
        def registrationCode = token ? RegistrationCode.findByToken(token) : null
        println "registration code is" + registrationCode
        //TODO check if passwords are equal

        if (!registrationCode) {
            flash.error = message(code: 'spring.security.ui.register.badCode')
            redirect uri: defaultTargetUrl
            return
        }

        def user


        RegistrationCode.withTransaction { status ->
            String usernameFieldName = SpringSecurityUtils.securityConfig.userLookup.usernamePropertyName
            user = lookupUserClass().findWhere((usernameFieldName): registrationCode.username)
            if (!user) {
                return
            }
            user.accountLocked = false

            String salt = saltSource instanceof NullSaltSource ? null : params.username
            user.password = springSecurityUiService.encodePassword(params.password1, salt)
            user.save(flush: true)
            def UserRole = lookupUserRoleClass()
            def Role = lookupRoleClass()
            for (roleName in conf.ui.register.defaultRoleNames) {
                UserRole.create user, Role.findByAuthority(roleName)
            }
            registrationCode.delete()
        }

        if (!user) {
            flash.error = message(code: 'spring.security.ui.register.badCode')
            redirect uri: defaultTargetUrl
            return
        }

    }

    protected String generateLink(String action, linkParams) {
        createLink(base: "$request.scheme://$request.serverName:$request.serverPort$request.contextPath",
                controller: 'user', action: action,
                params: linkParams)
    }
}
